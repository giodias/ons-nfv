package nfvs;

import java.util.Set;

public interface ILinkE {
    void addRequest(Request r);
    void removeRequest(Request r);
    void removeRequests(Request[] r);
    int getSetupCost();
    double getAvailableBR();
    double getWeight();
    Set<Request> getRequests();
}
