package nfvs.disaster;

import ons.Link;

public class RepairData {
    public Link link;
    public double weight;

    public RepairData(Link link, double weight) {
        this.link = link;
        this.weight = weight;
    }
}
