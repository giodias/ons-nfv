package nfvs.disaster;

import ons.Link;
import ons.PhysicalTopology;
import java.util.*;

/**
 *  This class represents a Disaster Zone, a set of links with a certain
 *  probability of being destroyed and having the requisitions interrupted.
 */
public class DisasterZone {

    private static List<DisasterZone> zones;
    //private static List<Double> cprob;
    public  static PhysicalTopology pt;

    private long  id;
    private List<Link> links;
    private Map<Integer, Double> plinks;
    private boolean status;

    static {
        zones = new ArrayList<>();
        pt = null;
    }

    /**
     * Register a new disaster zone
     *
     * @param id Integer unique identifier for the disaster zone
     * @return A disaster zone object
     */
    public static synchronized DisasterZone registerZone(long id) {
        if(DisasterZone.getZone(id) != null) {
            return DisasterZone.zones.get((int)id);
        }
        DisasterZone z = new DisasterZone(id);
        DisasterZone.zones.add(z);
        return z;
    }

    public static double getLinkDisasterProbability(Link link) {
        for(DisasterZone zone : zones) {
            Double d = zone.plinks.get(link.getID());
            if(d != null) {
                return d;
            }
        }
        return 0.0;
    }

    /**
     * Gets a registered zone from its unique ID
     *
     * @param id Integer unique identifier for the disaster zone
     * @return A disaster zone object or null, if have no zone with this id
     */
    public static DisasterZone getZone(long id) {
        Iterator<DisasterZone> it = zones.iterator();
        while (it.hasNext()) {
            DisasterZone z = it.next();
            if(z.getId() == id) {
                return z;
            }
        }
        return null;
    }

    public static DisasterZone getRandomZone() {
        if(zones.isEmpty())
            return null;
        return zones.get(DisasterGenerator.rand.nextInt(zones.size()));
    }

    /**
     * DizasterZone class constructor
     *
     * @param id Integer unique identifier for the disaster zone
     */
    private DisasterZone(long id) {
        this.id = id;
        this.links = new ArrayList<>();
        this.plinks = new HashMap<>();
        this.status = true;
    }

    public  long getId() {
        return this.id;
    }

    //TODO: Check for duplicity
    public void addLink(int lid, double probability) {
        this.links.add(DisasterZone.pt.getLink(lid));
        this.plinks.put(lid,probability);
    }

    public synchronized Link[] getRandomLinks() {
        List<Link> link_list = new LinkedList<>();
        Iterator<Link> it = this.links.iterator();
        while (it.hasNext()) {
            Link l1 = it.next();
            Link l2 = pt.getLink(l1.getDestination(), l1.getSource());
            if(DisasterGenerator.rand.nextDouble() < plinks.get(l1.getID())){
                if((l1.getWeight() != Double.POSITIVE_INFINITY)&&(l2.getWeight() != Double.POSITIVE_INFINITY)) {
                    link_list.add(l1);
                    link_list.add(l2);
                }
            }
        }
        return link_list.toArray(new Link[0]);
    }

    public void setAlive() {
        status = true;
    }

    public void setAffected() {
        status = false;
    }

    public boolean getStatus() {
        return status;
    }

}
