package nfvs.mra;

import nfvs.*;
import nfvs.utils.Dijkstra.DijkstraDisaster2;
import ons.*;
import ons.ra.ControlPlaneForRA;
import ons.util.WeightedGraph;

import java.util.*;

public class EON_MRA_COS_SFC implements MRA {

    private ControlPlaneForRA cp;
    private WeightedGraph graph;
    private int modulation;
    private boolean inverted;
    Map<Flow, Path> mappedFlows;


    public EON_MRA_COS_SFC(){
        this.inverted = false;
    }

    public EON_MRA_COS_SFC(boolean inverted){
        this.inverted = inverted;
    }

    @Override
    public void simulationInterface(ControlPlaneForRA cp, Map<Flow, Path> mf) {
        this.cp = cp;
        this.graph = cp.getPT().getWeightedGraph();
        //Set the default modulation
        this.modulation = Modulation._BPSK;
        mappedFlows = mf;
    }

    @Override
    public int[] migrate(Collection<Request> requests, Link[] avoid) {
        if(avoid == null){
            avoid = new Link[0];
        }
        double[] old_w = new double[avoid.length];

        int[] m_class = new int[COS.getNumCos()];
        for(int i = 0; i < m_class.length; ++i) {
            m_class[i] = 0;
        }

        for(int i = 0; i < old_w.length; ++i) {
            old_w[i] = avoid[i].getWeight();
            avoid[i].setWeight(Double.POSITIVE_INFINITY);
        }

        List<Request> rqs  = new ArrayList<>(requests);
        if(inverted) {
            rqs.sort((Request a,  Request b) -> {
                if(a.getCOS() < b.getCOS()) {
                    return 1;
                }
                else if (a.getCOS() > b.getCOS()) {
                    return -1;
                }
                return 0;
            });
        }
        else {
            rqs.sort((Request a,  Request b) -> {
                if(a.getCOS() < b.getCOS()) {
                    return -1;
                }
                else if (a.getCOS() > b.getCOS()) {
                    return 1;
                }
                return 0;
            });
        }


        for(Request r : rqs) {
            if(flowMigrate(r)) {
                ++m_class[r.getCOS()];
            }
        }

        for(int i = 0; i < old_w.length; ++i) {
            avoid[i].setWeight(old_w[i]);
        }

        return m_class;
    }


    @Override
    public Request migrateFirst(Collection<Request> requests, Link[] avoid) {
        Request retval = null;

        if(avoid == null){
            avoid = new Link[0];
        }
        double[] old_w = new double[avoid.length];

        int[] m_class = new int[COS.getNumCos()];
        for(int i = 0; i < m_class.length; ++i) {
            m_class[i] = 0;
        }

        for(int i = 0; i < old_w.length; ++i) {
            old_w[i] = avoid[i].getWeight();
            avoid[i].setWeight(Double.POSITIVE_INFINITY);
        }

        List<Request> rqs  = new ArrayList<>(requests);
        if(inverted) {
            rqs.sort((Request a,  Request b) -> {
                if(a.getCOS() < b.getCOS()) {
                    return 1;
                }
                else if (a.getCOS() > b.getCOS()) {
                    return -1;
                }
                return 0;
            });
        }
        else {
            rqs.sort((Request a,  Request b) -> {
                if(a.getCOS() < b.getCOS()) {
                    return -1;
                }
                else if (a.getCOS() > b.getCOS()) {
                    return 1;
                }
                return 0;
            });
        }

        for(Request r : rqs) {
            if(flowMigrate(r)) {
                retval = r;
                break;
            }
        }

        for(int i = 0; i < old_w.length; ++i) {
            avoid[i].setWeight(old_w[i]);
        }

        return retval;
    }

    public boolean flowMigrate(Request request) {
        DijkstraDisaster2 adj = new DijkstraDisaster2();
        int[] nodes;
        int[] selectedDatacenters = new int[request.getRequestedFunctions().length];
        List<Integer> datacentersPath = new ArrayList<>();

        VNF[] used_vnf = request.getUsedFunctions();

        for(int i = 0; i < selectedDatacenters.length; ++i) {
            selectedDatacenters[i] = used_vnf[i].getDatacenter().getID();
        }

        //Stage 2
        int src = request.getSource();
        datacentersPath.add(src);
        for(int dst: selectedDatacenters) {
            nodes = adj.getKShortestPath(graph, src, dst, request.getCOS(), cp.getPT(), request.getRate());
            if((nodes.length < 1)) {
                return false;
            }
            for(int n = 1; n < nodes.length; ++n) {
                datacentersPath.add(nodes[n]);
            }
            src = dst;
        }
        nodes = adj.getKShortestPath(graph, src, request.getDestination(), request.getCOS(), cp.getPT(), request.getRate());
        if((nodes.length < 1)) {
            return false;
        }
        for(int n = 1; n < nodes.length; ++n) {
            datacentersPath.add(nodes[n]);
        }
        /*  Second Stage End */

        nodes = new int[datacentersPath.size()];
        for(int i = 0; i < nodes.length; ++i) {
            nodes[i] = datacentersPath.get(i);
        }
        datacentersPath.clear();

        // Create the links vector
        int[] links = new int[nodes.length - 1];
        for (int j = 0; j < links.length; j++) {
            EONLinkE link = (EONLinkE)cp.getPT().getLink(nodes[j], nodes[j + 1]);
            links[j] = link.getID();
        }

        for(int l : links) {
            if (cp.getPT().getLink(l).getWeight() >= Double.POSITIVE_INFINITY) {
                return false;
            }
        }

        // Calculates the required slots
        int requiredSlots = Modulation.convertRateToSlot(request.getRate(), EONPhysicalTopology.getSlotSize(), modulation);

        // Evaluate if each link have space to the required slots
        Map<Integer, Integer> dls = new HashMap<>();
        for (int link : links) {
            Integer av = dls.get(link);
            if (av == null) {
                av = ((EONLink) cp.getPT().getLink(link)).getAvaiableSlots();
                dls.put(link, av);
            }
            if ((av - requiredSlots) < 0) {
                return false;
            } else {
                dls.put(link, av - requiredSlots);
            }
        }

        // Intersection First-Fit
        Set<Integer> slot_set =  ((EONLink)cp.getPT().getLink(links[0])).getSlotsAvailable(requiredSlots);
        for(int i = 1; i < links.length; ++i) {
            slot_set.retainAll(((EONLink)cp.getPT().getLink(links[i])).getSlotsAvailable(requiredSlots));
        }

        LightPath[] lps = new LightPath[1];
        LightPath[] lpo = mappedFlows.get(request).getLightpaths();
        long id = lpo[0].getID();

        for(int firstSlot : slot_set) {
            // Now you create the lightpath to use the createLightpath VT
            EONLightPath lp = cp.createCandidateEONLightPath(request.getSource(), request.getDestination(), links,
                    firstSlot, (firstSlot + requiredSlots - 1), modulation);
            // Now you try to establish the new lightpath, accept the call
            if (cp.getVT().rerouteLightPath(id, lp)) {
                // Single-hop routing (end-to-end lightpath)
                lps[0] = cp.getVT().getLightpath(id);
                if (cp.rerouteFlow(request.getID(), lps)) {
                    return true;
                }
            }
        }
        if(!cp.getVT().rerouteLightPath(id, lpo[0])){
            System.err.println("Error!!!!!!");
            System.exit(1);
        }
        return false;
    }
}
