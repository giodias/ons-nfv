package nfvs.mra;

import nfvs.Request;
import ons.Flow;
import ons.Link;
import ons.Path;
import ons.ra.ControlPlaneForRA;

import java.util.Collection;
import java.util.Map;

public interface MRA {
    void simulationInterface(ControlPlaneForRA cp, Map<Flow, Path>mf);
    int[] migrate(Collection<Request> requests, Link[] avoid);
    Request migrateFirst(Collection<Request> requests, Link[] avoid);
    boolean flowMigrate(Request request);
}
