package nfvs;

import nfvs.disaster.DisasterGenerator;
import nfvs.events.DisasterEvent;

import java.util.*;

public class NFVStatistics {

    private static NFVStatistics singleton;
    public  static final double[] costs = {0.00000375, 0.000003, 0.0000015, 0.0};

    private  int    n_calls;
    private  int    blocked_by_DC;
    private  int    blocked_by_VNF;
    private  int    blocked_by_BW;
    private  int    blocked_by_Path;
    private  int    blocked_by_LP;
    private  double routing_costs;
    private  double routing_costs_n;
    private  double setup_costs;
    private  double setup_costs_n;
    private  List<Integer>  used_links;
    private  Map<Integer, Integer> instantiated_count;
    private  Map<Integer, Integer> required_count;
    private  int[] requestFailures;
    private  int[] recoveryRequests;
    private  int[] blockedRequestsByCOS;
    private  int[] acceptedRequestsByCOS;
    private  int[] migratedByCOS;
    private  double blocked_cost;
    private  double disaster_cost;

    static {
        NFVStatistics.singleton = null;
    }

    private NFVStatistics() {
        this.n_calls		 = -1;
        this.blocked_by_DC   = 0;
        this.blocked_by_VNF  = 0;
        this.blocked_by_BW   = 0;
        this.blocked_by_Path = 0;
        this.routing_costs   = 0;
        this.routing_costs_n = 0;
        this.setup_costs     = 0;
        this.setup_costs_n   = 0;
        this.used_links    = new ArrayList<Integer>();
        this.instantiated_count = new HashMap<Integer, Integer>();
        this.required_count = new HashMap<Integer, Integer>();
        this.requestFailures = new int[COS.getNumCos() | 4];
        this.recoveryRequests = new int[COS.getNumCos() | 4];
        this.blockedRequestsByCOS = new int[COS.getNumCos() | 4];
        this.acceptedRequestsByCOS = new int[COS.getNumCos() | 4];
        this.migratedByCOS = new int[COS.getNumCos() | 4];
        for(int i = 0; i < requestFailures.length; ++i) {
            requestFailures[i] = 0;
            blockedRequestsByCOS[i] = 0;
            acceptedRequestsByCOS[i] = 0;
            recoveryRequests[i] = 0;
            migratedByCOS[i] = 0;
        }
        blocked_cost = 0;
        disaster_cost = 0;
    }

    public void clear() {
        //
        this.blocked_by_DC   = 0;
        this.blocked_by_VNF  = 0;
        this.blocked_by_BW   = 0;
        this.blocked_by_Path = 0;
        this.blocked_by_LP   = 0;
        this.routing_costs   = 0;
        this.routing_costs_n = 0;
        this.setup_costs     = 0;
        this.setup_costs_n   = 0;
        this.used_links.clear();
        this.instantiated_count.clear();
        this.required_count.clear();
        for(int i = 0; i < requestFailures.length; ++i) {
            requestFailures[i] = 0;
            blockedRequestsByCOS[i] = 0;
            acceptedRequestsByCOS[i] = 0;
            recoveryRequests[i] = 0;
            migratedByCOS[i] = 0;
        }
        blocked_cost = 0;
        disaster_cost = 0;
    }

    public synchronized static NFVStatistics getObject() {
        if(NFVStatistics.singleton == null) {
            NFVStatistics.singleton = new NFVStatistics();
        }
        return NFVStatistics.singleton;
    }

    public void setNumberOfCalls(int n_calls) {
        this.n_calls = n_calls;
    }

    public int getNumberOfCalls() {
        return this.n_calls;
    }

    public static void accept(Request req, double routingCost, Collection<Integer> instatiatedFunctions, int usedLinks, double setupCost) {
        singleton.addRoutingCost(routingCost);
        singleton.funtionRequired(req.getRequestedFunctions());
        singleton.functionInstantiated(instatiatedFunctions);
        singleton.addUsedLinks(usedLinks);
        singleton.addSetupCost(setupCost);

        singleton.acceptedRequestsByCOS[req.getCOS()]++;
    }

    public void blocked(Request rq) {
        this.blocked_cost += costs[rq.getCOS()] * 1500 * (rq.getRate()/1000.0) * rq.getDuration();
        this.blockedRequestsByCOS[rq.getCOS()]++;
    }

    public void blockedByDC(Request rq) {
        blocked(rq);
        this.blocked_by_DC++;
    }

    public void blockedByBW(Request rq){
        blocked(rq);
        this.blocked_by_BW++;
    }

    public void blockedByPath(Request rq) {
        blocked(rq);
        this.blocked_by_Path++;
    }

    public void blockedByVNF(Request rq) {
        blocked(rq);
        this.blocked_by_VNF++;
    }

    public void blockedByLP(Request rq) {
        blocked(rq);
        this.blocked_by_LP++;
    }

    public void addRoutingCost(double cost) {
        this.routing_costs += cost;
        this.routing_costs_n++;
    }

    public void addRoutingCost(Collection<Double> costs) {
        Iterator<Double> it = costs.iterator();
        while(it.hasNext()) {
            this.routing_costs = ((this.routing_costs*this.routing_costs_n)+it.next())/(++this.routing_costs_n);
        }
    }

    public void addSetupCost(double sc) {
        this.setup_costs += sc;
        this.setup_costs_n++;
    }

    public void addUsedLinks(int n) {
        this.used_links.add(n);
    }

    public void addRequestsfailures(Request rq) {
        requestFailures[rq.getCOS()] += 1;
        disaster_cost += costs[rq.getCOS()] * 1500 * (rq.getRate()/1000.0) * rq.getDuration();
    }

    public void addRequestRecovery(Request rq) {
        recoveryRequests[rq.getCOS()] += 1;
    }

    public void functionInstantiated(int vnf) {
        Integer c = this.instantiated_count.get(vnf);
        if(c == null) {
            this.instantiated_count.put(vnf, 1);
        }
        else {
            this.instantiated_count.put(vnf, c + 1);
        }
    }

    public void addMigration(int cos) {
        ++migratedByCOS[cos];
    }

    public void addMigration(int[] migratedbycos) {
        for(int i = 0; i < migratedbycos.length; ++i)
            ++migratedByCOS[i];
    }

    public void functionInstantiated(Collection<Integer> vnfs) {
        Integer c;
        for(int vnf: vnfs) {
            c = this.instantiated_count.get(vnf);
            if(c == null) {
                this.instantiated_count.put(vnf, 1);
            }
            else {
                this.instantiated_count.put(vnf, c + 1);
            }
        }
    }

    public void funtionRequired(int[] vnfs) {
        Integer c;
        for(int vnf: vnfs) {
            c = this.required_count.get(vnf);
            if(c == null) {
                c = 0;
            }
            this.required_count.put(vnf, c + 1);
        }
    }

    public synchronized double getAverageRoutingCost() {
        return (this.routing_costs/this.routing_costs_n);
    }

    public synchronized double getTotalRoutingCost() {
        return this.routing_costs;
    }

    public int getTotalUsedLinks() {
        int accumulator = 0;
        for(int n : this.used_links) {
            accumulator += n;
        }
        return accumulator;
    }

    public double getAverageUsedLinks() {
        if(this.used_links.size() == 0) {
            return Double.NaN;
        }
        return (this.getTotalUsedLinks() / this.used_links.size());
    }

    public double getTotalSetupCost() {
        return this.setup_costs;
    }

    public double getAverageSetupCost() {
        if(this.setup_costs_n == 0) {
            return Double.NaN;
        }
        return (this.setup_costs/this.setup_costs_n);
    }

    public int getBlocked() {
        return (this.blocked_by_DC + this.blocked_by_BW + this.blocked_by_Path + this.blocked_by_VNF + this.blocked_by_LP);
    }

    public int getAccepted() {
        int acc = 0;
        for(int a : acceptedRequestsByCOS) {
            acc += a;
        }
        return acc;
    }

    public int getFailures() {
        int acc = 0;
        for(int a : requestFailures) {
            acc += a;
        }
        return  acc;
    }

    public int getBlockedByVNF() {
        return this.blocked_by_VNF;
    }

    public int getBlockedByDC() {
        return this.blocked_by_DC;
    }

    public int getBlockedByBW() {
        return this.blocked_by_BW;
    }

    public int getBlockedByPath() {
        return this.blocked_by_Path;
    }

    public int getBlockedByLP() {
        return this.blocked_by_LP;
    }

    public void printStatistics(int simType) {
        int a = 0;
        int total_blocked = getBlocked();
        int total = total_blocked + getAccepted();
        int accepted = getAccepted();
        int failure = getFailures();
        int numOfCall = getNumberOfCalls();
        System.out.println("-----------------------------------");
        System.out.println("           NFV Statistics          ");
        System.out.println("Number of Requests: " + numOfCall);
        System.out.println("Alive Requests: " + (accepted - failure));
        System.out.println("Accepted Requests: " + accepted);
        System.out.println("Accepted by COS: ");
        for(int i = 0; i < acceptedRequestsByCOS.length; ++i) {
            System.out.println("    COS "+i+": "+acceptedRequestsByCOS[i]);
        }
        System.out.println("Blocked Requests: " + total_blocked);
        System.out.println("Blocked by DC: " + (100.0*this.getBlockedByDC()/total) + "%");
        System.out.println("Blocked by VNF: " + (100.0*this.getBlockedByVNF()/total) + "%");
        System.out.println("Blocked by BW: " + (100.0*this.getBlockedByBW()/total) + "%");
        System.out.println("Blocked by Path: " + (100.0*this.getBlockedByPath()/total) + "%");
        System.out.println("Blocked by LP: "+ (100.0*this.getBlockedByLP()/total) + "%");
        System.out.println("Blocked by COS: ");
        for(int i = 0; i < blockedRequestsByCOS.length; ++i) {
            System.out.println("    COS "+i+": "+blockedRequestsByCOS[i]);
        }
        System.out.println("Routing Cost: " + this.getTotalRoutingCost());
        System.out.println("Routing Cost Average: " + this.getAverageRoutingCost());
        System.out.println("Used Links: " + this.getTotalUsedLinks());
        System.out.println("Average of Used Links: " + this.getAverageUsedLinks());
        System.out.println("Setup Costs: " + this.getTotalSetupCost());
        System.out.println("Average of Setup Costs: " + this.getAverageSetupCost());
        System.out.println("Satisfied Functions:");
        for(int vnf: VNF.all_functions) {
            Integer c = this.required_count.get(vnf);
            if(c==null)
                c = 0;
            System.out.printf("    VNF %d: %d\n",vnf, c);
            a += c;
        }
        System.out.printf("    Total Satisfied Functions: %d\n", a);

        System.out.println("Instanced Functions:");
        a = 0;
        for(int vnf: VNF.all_functions) {
            Integer c = this.instantiated_count.get(vnf);
            if(c==null)
                c = 0;
            System.out.printf("    VNF %d: %d\n",vnf, c);
            a += c;
        }
        System.out.printf("    Total Instanced Functions: %d\n", a);

        int[] affected = new int[acceptedRequestsByCOS.length];

        System.out.println("Disasters - Affected");
        for(int i = 0; i < requestFailures.length; ++i) {
            affected[i] = (requestFailures[i] + recoveryRequests[i]);
            System.out.println("AD-"+i+" "+((100.0 * affected[i])/acceptedRequestsByCOS[i])+"%");
        }
        System.out.println("Disasters - Down Requests");
        for(int i = 0; i < requestFailures.length; ++i) {
            System.out.println("DP-" + i + " " + ((affected[i]==0)?(0.0):((100.0*requestFailures[i])/affected[i]))+"%");
        }
        System.out.println("Disasters - Recovery Requests");
        int recovery = 0;
        for(int i = 0; i < requestFailures.length; ++i) {
            System.out.println("RP-" + i + " " + ((affected[i]==0)?(0.0):((100.0*recoveryRequests[i])/affected[i]))+"%");
            recovery += recoveryRequests[i];
        }
        System.out.println("Disasters - Migrated Requests");
        for(int i = 0; i < requestFailures.length; ++i) {
            System.out.println("MP-" + i + " " + ((acceptedRequestsByCOS[i]==0)?(0.0):((100.0*migratedByCOS[i])/acceptedRequestsByCOS[i]))+"%");
            recovery += recoveryRequests[i];
        }
        System.out.println("Total Failure: " + (failure + recovery));
        System.out.println("Total Recovery: "+recovery);
        System.out.println("Blocked Cost: " + (Math.round(blocked_cost*100.0)/100.0) + "$");
		System.out.println("Disaster Cost: " + (Math.round(disaster_cost*100.0)/100.0) + "$");
		System.out.println("Total Cost: " + (Math.round((disaster_cost + blocked_cost)*100.0)/100.0 )+ "$");
    }
}
