package nfvs.utils.Dijkstra;

import nfvs.ILinkE;
import nfvs.disaster.DisasterZone;
import ons.EONLink;
import ons.EONPhysicalTopology;
import ons.Link;
import ons.WDMLink;
import ons.util.WeightedGraph;

public class DijkstraDisaster extends DijkstraExtra {

    public double getDijkstraWeight(WeightedGraph G, Link link, int r) {
        double ldp = (DisasterZone.getLinkDisasterProbability(link)*100);
        double lsc = (((ILinkE)link).getSetupCost());
        double abr = ((ILinkE)link).getAvailableBR();
        if(link.getWeight() >= Double.POSITIVE_INFINITY) {
            return Double.POSITIVE_INFINITY;
        }
        //
        if(link instanceof WDMLink) {
            if(((WDMLink)link).hasBWAvailable(r).length <= 0) {
                return Double.POSITIVE_INFINITY;
            }
        }
        //EON
        else if(r > (((EONLink)link).getAvaiableSlots() * EONPhysicalTopology.getSlotSize())) {
                return Double.POSITIVE_INFINITY;
        }
        //
        if(abr == 0) {
            return Double.POSITIVE_INFINITY;
        }
        return ((lsc + (r/abr)) * (1 + ldp));
        //C  SetupCost * (1 + disaster probability)
        //N  SetupCost * (rBand) * (1 + disaster probability)
    }
}
