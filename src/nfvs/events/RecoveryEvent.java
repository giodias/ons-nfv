package nfvs.events;

import java.util.*;

import nfvs.NFVStatistics;
import nfvs.Request;
import nfvs.rra.RRA;
import ons.Flow;
import ons.FlowDepartureEvent;

public class RecoveryEvent extends EventE {

    public static Class rraClass;

    private Set<Request> requests;

    public RecoveryEvent() {
        requests = new HashSet<>();
    }

    public void addRequest(Request request) {
        requests.add(request);
    }

    public void addRequests(Collection<Request> r) {
        requests.addAll(r);
    }

    public Flow[] getFlows() {
        return requests.toArray(new Flow[0]);
    }

    public Request[] getRequests() {
        return requests.toArray(new Request[0]);
    }

    public Request[] execute() {
        RRA rra = null;
        if(rraClass != null) {
            try {
                rra = (RRA) rraClass.newInstance();
                rra.simulationInterface(cp);
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
                return requests.toArray(new Request[0]);
            }
        }
        else {
            //If no recovery method, drop all
            return requests.toArray(new Request[0]);
        }
        List<Request> rqs  = new ArrayList<>(requests);
        rqs.sort((Request a,  Request b) -> {
            if(a.getCOS() < b.getCOS())
                return -1;
            else if (a.getCOS() > b.getCOS())
                return 1;
            return 0;
        });
        for(Request r : rqs) {
            if(!r.getFlag(Request.Flag.POSTPONED)) {
                //TODO: Only EON
                r.degradeBW(getTime(), true);
            }
        }
        Request[][] aux = rra.recovery(rqs);

        for(Request r : aux[0]) {
            //Success
            FlowDepartureEvent fde = r.getDepartureEvent();
            fde.setTime(getTime() + r.getRemainDuration());
            eventScheduler.addEvent(fde);
            NFVStatistics.getObject().addRequestRecovery(r);
        }

        List<Request> hard_fail = new ArrayList<>();
        for(Request r : aux[1]) {
            //Failure
            double timep = r.getTimePercentualTolerance();
            if(r.getFlag(Request.Flag.POSTPONED) || (timep <= 0.0)) {
                hard_fail.add(r);
            }
            else{
                RecoveryEvent nre = new RecoveryEvent();
                nre.setTime(getTime() + (r.getTimePercentualTolerance() * r.getRemainDuration()));
                nre.addRequest(r);
                r.setFlag(Request.Flag.POSTPONED);
                eventScheduler.addEvent(nre);
            }
        }

        //Update Graph
        cp.getRA().simulationInterface(cp);
        //rra.simulationInterface(cp);
        return hard_fail.toArray(new Request[0]);
    }

}
