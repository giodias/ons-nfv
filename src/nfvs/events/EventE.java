package nfvs.events;

import ons.ControlPlane;
import ons.Event;
import ons.EventScheduler;

public abstract class EventE extends Event {
    //public static List<Link> interrupted;
    protected static EventScheduler eventScheduler;
    protected static ControlPlane cp;

    static {
        eventScheduler = null;
        cp = null;
        //interrupted = new LinkedList<>();
    }

    public static void setEventScheduler(EventScheduler es) {
        eventScheduler = es;
    }

    public static void setCP(ControlPlane cp) {
        EventE.cp = cp;
    }

    public static void adjustEventTime(Event event, double time) {
        eventScheduler.removeEvent(event);
        event.setTime(time);
        eventScheduler.addEvent(event);
    }
}
