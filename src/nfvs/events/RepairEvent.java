package nfvs.events;

import nfvs.disaster.DisasterZone;
import nfvs.disaster.RepairData;
import ons.ControlPlane;
import ons.Event;
import ons.Link;

public class RepairEvent extends Event {
    private static ControlPlane cp;

    private RepairData[] affectedLinks;
    private DisasterZone zone;

    public static void setCP(ControlPlane cp) {
        RepairEvent.cp = cp;
    }

    public void setLinks(Link[] links) {
        affectedLinks = new RepairData[links.length];
        for(int i = 0; i < links.length; ++i) {
            affectedLinks[i] = new RepairData(links[i], links[i].getWeight());
        }
    }

    public void setZone(DisasterZone zone) {
        this.zone = zone;
    }

    public void execute() {
        for(RepairData rd : affectedLinks) {
            rd.link.setWeight(rd.weight);
        }
        zone.setAlive();
        //Update Graph
        cp.getRA().simulationInterface(cp);
    }

}
