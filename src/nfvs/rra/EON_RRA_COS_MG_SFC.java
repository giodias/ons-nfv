package nfvs.rra;

import nfvs.Datacenter;
import nfvs.EONLinkE;
import nfvs.Request;
import nfvs.VNF;
import nfvs.utils.Dijkstra.DijkstraDisaster2;
import ons.*;
import ons.ra.ControlPlaneForRA;
import ons.util.WeightedGraph;

import java.util.*;

public class EON_RRA_COS_MG_SFC implements RRA{

    private ControlPlaneForRA cp;
    private WeightedGraph graph;
    private int modulation;

    @Override
    public void simulationInterface(ControlPlaneForRA cp) {
        this.cp = cp;
        this.graph = cp.getPT().getWeightedGraph();
        //Set the default modulation
        this.modulation = Modulation._BPSK;
    }

    @Override
    public Request[][] recovery(Collection<Request> requests) {
        List<Request> fails  = new ArrayList<>();
        List<Request> success = new ArrayList<>();
        for(Request req : requests) {
            if(!flowRecovery(req)){
                fails.add(req);
            }
            else {
                success.add(req);
            }
        }
        Request[][] ret = new Request[2][];

        ret[0] = success.toArray(new Request[0]);
        ret[1] = fails.toArray(new Request[0]);
        return ret;
    }

    public boolean flowRecovery(Request request) {
        int[] nodes;
        int[] links;
        long id;
        LightPath[] lps = new LightPath[1];

        DijkstraDisaster2 adj = new DijkstraDisaster2();

        List<Integer> selectedDatacenters = new ArrayList<>();
        List<Integer> datacentersPath = new ArrayList<>();
        List<Integer> instatiatedFunctions = new ArrayList<>();
        double rout_costs = 0;
        double setup_costs = 0;
        int fidx = 0;

        double[] distS  = adj.getAllHops(this.graph, request.getSource());
        double[] distD  = adj.getAllHops(this.graph, request.getDestination());

        double[] dist = new double[distS.length];
        for(int i = 0; i < dist.length; ++i) {
            dist[i] = (distS[i] < distD[i])?distS[i]:distD[i];
        }
        //distD = null;
        /*  First Stage Begin */
        for(int function: request.getRequestedFunctions()) {
            Datacenter min_dc = null;
            Datacenter current_dc;
            double min_setup_cost = Double.POSITIVE_INFINITY;
            double min_dist = Double.POSITIVE_INFINITY;
            double current_cost;
            double current_dist;
            VNF vnf;
            for(int k = 0; k < cp.getPT().getNumNodes(); k++) {
                current_dc = (Datacenter) cp.getPT().getNode(k);
                current_cost = (double) current_dc.implementAtCost(function);
                current_dist = dist[current_dc.getID()];
                if((current_cost < min_setup_cost)) {
                    if (current_dc.hasResourcesTo(function) || (current_dc.hasInstantiatedFunctionWithCapacity(function) != null)) {
                        min_setup_cost = current_cost;
                        min_dc = current_dc;
                        min_dist = current_dist;
                    }
                }
                else if ((current_cost == min_setup_cost) && (current_dist < min_dist) && (current_dc.hasResourcesTo(function) || (current_dc.hasInstantiatedFunctionWithCapacity(function) != null))) {
                    min_setup_cost = current_cost;
                    min_dc = current_dc;
                    min_dist = current_dist;
                }
            }
            if(min_dc == null) {
                //blocked By DC
                request.freeFunctions();
                return false;
            }
            vnf = min_dc.hasInstantiatedFunctionWithCapacity(function);
            if(vnf == null) {
                vnf = min_dc.instantiateFunction(function);
                if(vnf == null) {
                    //blocked By VNF
                    request.freeFunctions();
                    return false;
                }
                instatiatedFunctions.add(vnf.getID());
                setup_costs += vnf.getSetupCost();
            }
            vnf.newRequest();
            request.setUsedFunction(fidx++, vnf);
            selectedDatacenters.add(min_dc.getID());
        }
        /*  First Stage End */

        /*  Second Stage Begin */
        int src = request.getSource();
        datacentersPath.add(src);
        for(int dst: selectedDatacenters) {
            /*  Second Stage */
            //if(!datacentersPath.contains(dst)) {
                nodes = adj.getKShortestPath(graph, src, dst, request.getCOS(), cp.getPT(), request.getRate());
                if((nodes.length < 1)) {
                    //blocked By Path
                    request.freeFunctions();
                    return false;
                }
                for(int n = 1; n < nodes.length; ++n) {
                    datacentersPath.add(nodes[n]);
                }
                src = dst;
            //}
        }

        nodes = adj.getKShortestPath(graph, src, request.getDestination(), request.getCOS(), cp.getPT(), request.getRate());
        if((nodes.length < 1)) {
            //blocked By Path
            request.freeFunctions();
            return false;
        }
        for(int n = 1; n < nodes.length; ++n) {
            datacentersPath.add(nodes[n]);
        }
        /*  Second Stage End */

        nodes = new int[datacentersPath.size()];
        for(int i = 0; i < nodes.length; ++i) {
            nodes[i] = datacentersPath.get(i);
        }
        datacentersPath.clear();

        // Create the links vector
        links = new int[nodes.length - 1];
        for (int j = 0; j < links.length; j++) {
            EONLinkE link = (EONLinkE)cp.getPT().getLink(nodes[j], nodes[j + 1]);
            rout_costs += ((double)link.getSetupCost()) +  ((double)request.getRate() / (link.getAvaiableSlots()* EONPhysicalTopology.getSlotSize()));
            links[j] = link.getID();
        }

        for(int l : links) {
            if (cp.getPT().getLink(l).getWeight() >= Double.POSITIVE_INFINITY) {
                //blocked By Path
                request.freeFunctions();
                return false;
            }
        }

        // Calculates the required slots
        int requiredSlots = Modulation.convertRateToSlot(request.getRate(), EONPhysicalTopology.getSlotSize(), modulation);

        // Evaluate if each link have space to the required slots
        Map<Integer, Integer> dls = new HashMap<Integer, Integer>();
        for (int i = 0; i < links.length; i++) {
            Integer av = dls.get(links[i]);
            if(av == null) {
                av = ((EONLink) cp.getPT().getLink(links[i])).getAvaiableSlots();
                dls.put(links[i], av);
            }
            if ((av - requiredSlots) < 0) {
                //blocked By BW
                request.freeFunctions();
                return false;
            }
            else {
                dls.put(links[i], av - requiredSlots);
            }
        }

        //Migrate Classes
        ((ControlPlane) cp).migrateAllAboveClassFromLink(request.getCOS() - 1, links);

        // Intersection First-Fit
        Set<Integer> slot_set =  ((EONLink)cp.getPT().getLink(links[0])).getSlotsAvailable(requiredSlots);
        for(int i = 1; i < links.length; ++i) {
            slot_set.retainAll(((EONLink)cp.getPT().getLink(links[i])).getSlotsAvailable(requiredSlots));
        }

        for(int firstSlot : slot_set) {
            // Now you create the lightpath to use the createLightpath VT
            EONLightPath lp = cp.createCandidateEONLightPath(request.getSource(), request.getDestination(), links,
                    firstSlot, (firstSlot + requiredSlots - 1), modulation);
            // Now you try to establish the new lightpath, accept the call
            if ((id = cp.getVT().createLightpath(lp)) >= 0) {
                // Single-hop routing (end-to-end lightpath)
                lps[0] = cp.getVT().getLightpath(id);
                if (cp.acceptFlow(request.getID(), lps)) {
                    return true;
                } else {
                    // Something wrong
                    // Dealocates the lightpath in VT and try again
                    cp.getVT().deallocatedLightpath(id);
                }
            }
        }
        //Blocked by LP
        request.freeFunctions();
        return false;
    }
}
