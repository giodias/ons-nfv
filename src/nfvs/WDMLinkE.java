package nfvs;

import ons.WDMLink;

import java.util.LinkedHashSet;
import java.util.Set;

public class WDMLinkE extends WDMLink implements ILinkE{
	
	public int setup_cost;
	private Set<Request> activeRequests;
	
	public WDMLinkE(int id, int src, int dst, double delay, double weight, int wavelengths, int bw, int cost_min, int cost_max) {
		super(id, src, dst, delay, weight, wavelengths, bw);
		this.setup_cost = NFVRandom.nextIntInterval(cost_min, cost_max);
		this.activeRequests = new LinkedHashSet<>();
	}

	public WDMLinkE(int id, int src, int dst, double delay, double weight, int wavelengths, int bw, int setup_cost) {
		super(id, src, dst, delay, weight, wavelengths, bw);
		this.setup_cost = setup_cost;
		this.activeRequests = new LinkedHashSet<>();
	}

	public int getSetupCost() {
		return this.setup_cost;
	}

	@Override
	public void addRequest(Request r) {
		this.activeRequests.add(r);
	}

	@Override
	public void removeRequest(Request r) {
		this.activeRequests.remove(r);
	}

	@Override
	public void removeRequests(Request[] r) {
		for(Request rq : r) {
			removeRequest(rq);
		}
	}

	@Override
	public double getAvailableBR() {
		double abw = 0;
		for(int wl = 0; wl < getWavelengths(); ++wl) {
			double aux = amountBWAvailable(wl);
			if(aux > abw) {
				abw = aux;
			}
		}
		return abw;
	}

	@Override
	public Set<Request> getRequests() {
		return this.activeRequests;
	}
}
