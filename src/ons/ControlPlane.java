/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ons;

import nfvs.DisasterPredictor;
import nfvs.ILinkE;
import nfvs.NFVStatistics;
import nfvs.Request;
import nfvs.events.*;
import nfvs.mra.EON_MRA_COS_SFC;
import nfvs.mra.MRA;
import ons.ra.RA;
import ons.ra.ControlPlaneForRA;

import java.util.*;

/**
 * The Control Plane is responsible for managing resources and
 * connection within the network.
 * 
 * @author onsteam
 */
public class ControlPlane implements ControlPlaneForRA { // RA is Routing Assignment Problem

    private RA ra;
    private PhysicalTopology pt;
    private VirtualTopology vt;
    private Map<Flow, Path> mappedFlows; // Flows that have been accepted into the network
    private Map<Long, Flow> activeFlows; // Flows that have been accepted or that are waiting for a RA decision 
    private Tracer tr = Tracer.getTracerObject();
    private MyStatistics st = MyStatistics.getMyStatisticsObject();

    /**
     * Creates a new ControlPlane object.
     * 
     * @param raModule the name of the RA class
     * @param pt the network's physical topology
     * @param vt the network's virtual topology
     */
    public ControlPlane(String raModule, PhysicalTopology pt, VirtualTopology vt) {
        Class RAClass;

        mappedFlows = new HashMap<Flow, Path>();
        activeFlows = new HashMap<Long, Flow>();

        this.pt = pt;
        this.vt = vt;

        try {
            RAClass = Class.forName(raModule);
            ra = (RA) RAClass.newInstance();
            ra.simulationInterface(this);
        } catch (Throwable t) {
            t.printStackTrace();
        }

    }

    /**
     * Deals with an Event from the event queue.
     * If it is of the FlowArrivalEvent kind, adds it to the list of active flows.
     * If it is from the FlowDepartureEvent, removes it from the list.
     * 
     * @param event the Event object taken from the queue 
     */
    public void newEvent(Event event) {
        if (event instanceof FlowArrivalEvent) {
            newFlow(((FlowArrivalEvent) event).getFlow());
            ra.flowArrival(((FlowArrivalEvent) event).getFlow());
        } else if (event instanceof FlowDepartureEvent) {
            ra.flowDeparture(((FlowDepartureEvent) event).getID());
            removeFlow(((FlowDepartureEvent) event).getID());
        } else if (event instanceof DisasterEvent) {
            for (Request rq : (((DisasterEvent) event).execute())) {
                ra.flowDeparture(rq.getID());
                removeFlow(rq.getID());
            }
        }
        else if (event instanceof RepairEvent) {
            ((RepairEvent)event).execute();
        }
        else if (event instanceof RecoveryEvent) {
            Request[] fails;
            if(RecoveryEvent.rraClass != null) {
                //Bandwidth Degrade
                //TODO: WDM
                for (Flow f : mappedFlows.keySet()) {
                    if (f instanceof Request) {
                        EONLightPath lp = (EONLightPath) mappedFlows.get(f).getLightpaths()[0];
                        int oldRequiredSlots = Modulation.convertRateToSlot(f.getRate(), EONPhysicalTopology.getSlotSize(), lp.getModulation());
                        ((Request) f).degradeBW(event.getTime(), true);
                        int requiredSlots = Modulation.convertRateToSlot(f.getRate(), EONPhysicalTopology.getSlotSize(), lp.getModulation());
                        if(oldRequiredSlots == requiredSlots) {
                            continue;
                        }
                        //Degrade in PT
                        int firstSlot = lp.getFirstSlot();
                        LightPath[] lps = new LightPath[1];
                        EONLightPath nlp = createCandidateEONLightPath(f.getSource(), f.getDestination(), lp.getLinks(), firstSlot, (firstSlot + requiredSlots - 1), lp.getModulation());

                        long id = mappedFlows.get(f).getLightpaths()[0].id;
                        if (getVT().rerouteLightPath(id, nlp)) {
                            lps[0] = getVT().getLightpath(id);
                            // Single-hop routing (end-to-end lightpath)
                            if (rerouteFlow(f.getID(), lps)) {

                            } else {
                                // Something wrong
                                // Dealocates the lightpath in VT
                                //getVT().deallocatedLightpath(id);
                                //NFVStatistics.getObject().addRequestsfailures((Request) f);
                                System.err.println("Falha ao degradar requisição! Reroute Flow");
                                System.exit(1);
                            }
                        } else {
                            System.err.println("Falha ao degradar requisição! Reroute LP");
                            System.exit(2);
                        }
                    }
                }
                //Recovery
                //Active Flows
                for (Flow f : ((RecoveryEvent) event).getFlows()) {
                    newFlow(f);
                }

                fails = ((RecoveryEvent) event).execute();
            }
            else {
                fails = ((RecoveryEvent) event).getRequests();
            }
            //Remove Fail Requests from PT
            for(Request rq : fails) {
                NFVStatistics.getObject().addRequestsfailures(rq);
                removeFlow(rq.getID());
            }
        }
        else if(event instanceof MigrationEvent){
            //for(Flow f: mappedFlows.keySet()) {
                //
            //}
        }
        else if(event instanceof DisasterPredictionEvent) {
            Link[] predicted_links = ((DisasterPredictionEvent)event).execute();

            Set<Request> predicted_requests = new HashSet<>();

            if(DisasterPredictor.dpClass == null) {
                return;
            }

            for(Link l : predicted_links) {
                predicted_requests.addAll(((ILinkE)l).getRequests());
            }
            MRA mra;
            try {
                mra = (MRA) DisasterPredictor.dpClass.newInstance();
                mra.simulationInterface(this, mappedFlows);
            }catch (Exception e) {
                return;
            }

            NFVStatistics.getObject().addMigration(mra.migrate(predicted_requests, predicted_links));

        }

    }

    public int [] getLinksOfRequest(long id) {
        Request request;
        if (id < 0) {
            throw (new IllegalArgumentException());
        } else {
            if (!activeFlows.containsKey(id)) {
                return new int[0];
            }
            request = (Request) activeFlows.get(id);
            if (!mappedFlows.containsKey(request)) {
                return new int[0];
            }
            return mappedFlows.get(request).getLightpaths()[0].getLinks();
        }
    }

    public void defragLinks(int[] links) {
        PhysicalTopology pt = getPT();
        Set<Request> requests = new HashSet<>();
        for(int lid : links) {
            ILinkE link = (ILinkE)pt.getLink(lid);
            requests.addAll(link.getRequests());
        }
        for(Request request : requests) {
            int[] used_links = getLinksOfRequest(request.getID());
            EONLightPath lp = (EONLightPath)mappedFlows.get(request).getLightpaths()[0];
            reroute(request, this, used_links, lp.getModulation());
        }
    }

    public  boolean reroute(Request request, ControlPlaneForRA cp, int[] links, int modulation) {
        int requiredSlots = Modulation.convertRateToSlot(request.getRate(), EONPhysicalTopology.getSlotSize(), modulation);
        // Evaluate if each link have space to the required slots
        Map<Integer, Integer> dls = new HashMap<>();
        for (int link : links) {
            Integer av = dls.get(link);
            if (av == null) {
                av = ((EONLink) cp.getPT().getLink(link)).getAvaiableSlots();
                dls.put(link, av);
            }
            if ((av - requiredSlots) < 0) {
                return false;
            } else {
                dls.put(link, av - requiredSlots);
            }
        }

        // Intersection First-Fit
        Set<Integer> slot_set =  ((EONLink)cp.getPT().getLink(links[0])).getSlotsAvailable(requiredSlots);
        for(int i = 1; i < links.length; ++i) {
            slot_set.retainAll(((EONLink)cp.getPT().getLink(links[i])).getSlotsAvailable(requiredSlots));
        }

        LightPath[] lps = new LightPath[1];
        LightPath[] lpo = mappedFlows.get(request).getLightpaths();
        long id = lpo[0].getID();

        for(int firstSlot : slot_set) {
            // Now you create the lightpath to use the createLightpath VT
            EONLightPath lp = cp.createCandidateEONLightPath(request.getSource(), request.getDestination(), links,
                    firstSlot, (firstSlot + requiredSlots - 1), modulation);
            // Now you try to establish the new lightpath, accept the call
            if (cp.getVT().rerouteLightPath(id, lp)) {
                // Single-hop routing (end-to-end lightpath)
                lps[0] = cp.getVT().getLightpath(id);
                if (cp.rerouteFlow(request.getID(), lps)) {
                    return true;
                }
            }
        }
        if(!cp.getVT().rerouteLightPath(id, lpo[0])){
            System.err.println("Error!!!!!!");
            System.exit(1);
        }
        return false;
    }

    public void migrateAllOfClassFromLink(int cos, int[] lids) {
        PhysicalTopology pt = getPT();
        Set<Request> requests = new HashSet<>();
        Link[] llist = new Link[lids.length];
        int i = 0;
        for(int lid : lids) {
            ILinkE link = (ILinkE)pt.getLink(lid);
            for(Request r : link.getRequests()) {
                if(r.getCOS() == cos) {
                    requests.add(r);
                }
            }
            llist[i++] = (Link)link;
        }
        MRA mra = new EON_MRA_COS_SFC(true);
        mra.simulationInterface(this, mappedFlows);
        mra.migrate(requests, llist);
    }

    public void migrateAllAboveClassFromLink(int cos, int[] lids) {
        PhysicalTopology pt = getPT();
        Set<Request> requests = new HashSet<>();
        Link[] llist = new Link[lids.length];
        int i = 0;
        for(int lid : lids) {
            ILinkE link = (ILinkE)pt.getLink(lid);
            for(Request r : link.getRequests()) {
                if(r.getCOS() >= cos) {
                    requests.add(r);
                }
            }
            llist[i++] = (Link)link;
        }
        MRA mra = new EON_MRA_COS_SFC(true);
        mra.simulationInterface(this, mappedFlows);
        mra.migrate(requests, llist);
    }

    public void migrateAllAboveClassFromLink(int cos, int[] lids, int minBW) {
        PhysicalTopology pt = getPT();
        Set<Request> requests = new HashSet<>();
        Link[] llist = new Link[lids.length];
        int i = 0;
        for(int lid : lids) {
            ILinkE link = (ILinkE)pt.getLink(lid);
            for(Request r : link.getRequests()) {
                if((r.getCOS() >= cos) && (r.getRate() >= minBW)) {
                    requests.add(r);
                }
            }
            llist[i++] = (Link)link;
        }
        MRA mra = new EON_MRA_COS_SFC(true);
        mra.simulationInterface(this, mappedFlows);
        mra.migrate(requests, llist);
    }

    public void migrateFirstAboveClassFromLink(int cos, int[] lids, int minBW) {
        PhysicalTopology pt = getPT();
        Set<Request> requests = new HashSet<>();
        Link[] llist = new Link[lids.length];
        int i = 0;
        for(int lid : lids) {
            ILinkE link = (ILinkE)pt.getLink(lid);
            for(Request r : link.getRequests()) {
                if((r.getCOS() >= cos) && (r.getRate() >= minBW)) {
                    requests.add(r);
                }
            }
            llist[i++] = (Link)link;
        }
        MRA mra = new EON_MRA_COS_SFC(true);
        mra.simulationInterface(this, mappedFlows);
        mra.migrateFirst(requests, llist);
    }

    /**
     * Adds a given active Flow object to a determined Physical Topology.
     * 
     * @param id unique identifier of the Flow object
     * @param lightpaths the Path, or list of LighPath objects
     * @return true if operation was successful, or false if a problem occurred
     */
    @Override
    public boolean acceptFlow(long id, LightPath[] lightpaths) {
        Flow flow;

        if (id < 0 || lightpaths.length < 1) {
            throw (new IllegalArgumentException());
        } else {
            if (!activeFlows.containsKey(id)) {
                return false;
            }
            flow = activeFlows.get(id);
            if (!canAddFlowToPT(flow, lightpaths)) {
                return false;
            }
            if(!checkLightpathContinuity(flow, lightpaths)){
                return false;
            }
            int usedTransponders = 0;
            for (LightPath lightpath : lightpaths) {
                if(vt.isLightpathIdle(lightpath.getID())){
                    usedTransponders++;
                }
            }
            addFlowToPT(flow, lightpaths);
            mappedFlows.put(flow, new Path(lightpaths));
            tr.acceptFlow(flow, lightpaths);
            st.userTransponder(usedTransponders);
            st.acceptFlow(flow, lightpaths);
            return true;
        }
    }

    /**
     * Removes a given Flow object from the list of active flows.
     * 
     * @param id unique identifier of the Flow object
     * @return true if operation was successful, or false if a problem occurred
     */
    @Override
    public boolean blockFlow(long id) {
        Flow flow;

        if (id < 0) {
            throw (new IllegalArgumentException());
        } else {
            if (!activeFlows.containsKey(id)) {
                return false;
            }
            flow = activeFlows.get(id);
            if (mappedFlows.containsKey(flow)) {
                return false;
            }
            activeFlows.remove(id);
            tr.blockFlow(flow);
            st.blockFlow(flow);
            return true;
        }
    }
    
    /**
     * Removes a given Flow object from the Physical Topology and then
     * puts it back, but with a new route (set of LightPath objects). 
     * 
     * @param id unique identifier of the Flow object
     * @param lightpaths list of LightPath objects, which form a Path
     * @return true if operation was successful, or false if a problem occurred
     */
    @Override
    public boolean rerouteFlow(long id, LightPath[] lightpaths) {
        Flow flow;
        Path oldPath;

        if (id < 0 || lightpaths.length < 1) {
            throw (new IllegalArgumentException());
        } else {
            if (!activeFlows.containsKey(id)) {
                return false;
            }
            flow = activeFlows.get(id);
            if (!mappedFlows.containsKey(flow)) {
                return false;
            }
            oldPath = mappedFlows.get(flow);
            removeFlowFromPT(flow, lightpaths);

            //Remove Mapa de Requests dos Links
            {
                for(LightPath lp : oldPath.getLightpaths()){
                    for(int lid : lp.getLinks()) {
                        Request[] rqs = ((ILinkE)getPT().getLink(lid)).getRequests().toArray(new Request[0]);
                        ((ILinkE)getPT().getLink(lid)).removeRequests(rqs);
                    }
                }
            }

            if (!canAddFlowToPT(flow, lightpaths)) {
                addFlowToPT(flow, oldPath.getLightpaths());
                return false;
            }
            if(!checkLightpathContinuity(flow, lightpaths)){
                return false;
            }
            addFlowToPT(flow, lightpaths);
            mappedFlows.put(flow, new Path(lightpaths));
            //tr.flowRequest(id, true);
            return true;
        }
    }
    
    /**
     * Adds a given Flow object to the HashMap of active flows.
     * The HashMap also stores the object's unique identifier (ID). 
     * 
     * @param flow Flow object to be added
     */
    private void newFlow(Flow flow) {
        activeFlows.put(flow.getID(), flow);
    }
    
    /**
     * Removes a given Flow object from the list of active flows.
     * 
     * @param id the unique identifier of the Flow to be removed
     */
    private void removeFlow(long id) {
        Flow flow;
        LightPath[] lightpaths;

        if (activeFlows.containsKey(id)) {
            flow = activeFlows.get(id);
            if (mappedFlows.containsKey(flow)) {
                lightpaths = mappedFlows.get(flow).getLightpaths();
                removeFlowFromPT(flow, lightpaths);
                mappedFlows.remove(flow);
            }
            activeFlows.remove(id);
        }
    }
    
    /**
     * Removes a given Flow object from a Physical Topology. 
     * 
     * @param flow the Flow object that will be removed from the PT
     * @param lightpaths a list of LighPath objects
     */
    private void removeFlowFromPT(Flow flow, LightPath[] lightpaths) {
        for (LightPath lightpath : lightpaths) {
            pt.removeFlow(flow, lightpath);
            // Can the lightpath be removed?
            if (vt.isLightpathIdle(lightpath.getID())) {
                vt.removeLightPath(lightpath.getID());
            }
            //
            if(flow instanceof Request) {
                for(int lid: lightpath.getLinks()){
                    ((ILinkE)pt.getLink(lid)).removeRequest((Request) flow);
                }
            }
            //
        }
    }
    
    /**
     * Says whether or not a given Flow object can be added to a 
     * determined Physical Topology, based on the amount of bandwidth the
     * flow requires opposed to the available bandwidth.
     * 
     * @param flow the Flow object to be added 
     * @param lightpaths list of LightPath objects the flow uses
     * @return true if Flow object can be added to the PT, or false if it can't
     */
    private boolean canAddFlowToPT(Flow flow, LightPath[] lightpaths) {
        for (LightPath lightpath : lightpaths) {
            if (!pt.canAddFlow(flow, lightpath)) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Adds a Flow object to a Physical Topology.
     * This means adding the flow to the network's traffic,
     * which simply decreases the available bandwidth.
     * 
     * @param flow the Flow object to be added 
     * @param lightpaths list of LightPath objects the flow uses
     */
    private void addFlowToPT(Flow flow, LightPath[] lightpaths) {
        for (LightPath lightpath : lightpaths) {
            pt.addFlow(flow, lightpath);
            //
            if(flow instanceof Request) {
                for(int lid: lightpath.getLinks()){
                    ((ILinkE)pt.getLink(lid)).addRequest((Request) flow);
                }
            }
            //
        }
    }
    
    /**Checks the lightpaths continuity in multihop and if flow src and dst is equal in lightpaths
     * 
     * @param flow the flow requisition
     * @param lightpaths the set of lightpaths
     * @return true if evething is ok, false otherwise
     */
    private boolean checkLightpathContinuity(Flow flow, LightPath[] lightpaths) {
        if(flow.getSource() == lightpaths[0].getSource() && flow.getDestination() == lightpaths[lightpaths.length-1].getDestination()){
            for (int i = 0; i < lightpaths.length - 1; i++) {
                if(!(lightpaths[i].getDestination() == lightpaths[i+1].getSource())){
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Retrieves a Path object, based on a given Flow object.
     * That's possible thanks to the HashMap mappedFlows, which
     * maps a Flow to a Path.
     * 
     * @param flow Flow object that will be used to find the Path object
     * @return Path object mapped to the given flow 
     */
    @Override
    public Path getPath(Flow flow) {
        return mappedFlows.get(flow);
    }
    
    /**
     * Retrieves the complete set of Flow/Path pairs listed on the
     * mappedFlows HashMap.
     * 
     * @return the mappedFlows HashMap
     */
    @Override
    public Map<Flow, Path> getMappedFlows() {
        return mappedFlows;
    }
    
    /**
     * Retrieves a Flow object from the list of active flows.
     * 
     * @param id the unique identifier of the Flow object
     * @return the required Flow object
     */
    @Override
    public Flow getFlow(long id) {
        return activeFlows.get(id);
    }
    
    /**
     * Counts number of times a given LightPath object
     * is used within the Flow objects of the network.
     * 
     * @param id unique identifier of the LightPath object
     * @return integer with the number of times the given LightPath object is used
     */
    @Override
    public int getLightpathFlowCount(long id) {
        int num = 0;
        Path p;
        LightPath[] lps;
        ArrayList<Path> ps = new ArrayList<>(mappedFlows.values());
        for (Path p1 : ps) {
            p = p1;
            lps = p.getLightpaths();
            for (LightPath lp : lps) {
                if (lp.getID() == id) {
                    num++;
                    break;
                }
            }
        }
        return num;
    }
    
    /**
     * Retrieves the PhysicalTopology object
     * @return PhysicalTopology object
     */
    @Override
    public PhysicalTopology getPT(){
        return pt;
    }
    
    /**
     * Retrieves the VirtualTopology object
     * @return VirtualTopology object
     */
    @Override
    public VirtualTopology getVT(){
        return vt;
    }

    public RA getRA() {
        return ra;
    }

    /**
     * Creates a WDM LightPath candidate to put in the Virtual Topology (this method should be used by RA classes)
     * @param src the source node of the lightpath
     * @param dst the destination node of the lightpath
     * @param links the id links used by lightpath
     * @param wavelengths the wavelengths used by lightpath
     * @return the WDMLightPath object
     */
    @Override
    public WDMLightPath createCandidateWDMLightPath(int src, int dst, int[] links, int[] wavelengths) {
        return new WDMLightPath(1, src, dst, links, wavelengths);
    }

    /**
     * Creates a EON LightPath candidate to put in the Virtual Topology (this method should be used by RA classes)
     * @param src the source node of the lightpath
     * @param dst the destination node of the lightpath
     * @param links the id links used by lightpath
     * @param firstSlot the first slot used in this lightpath
     * @param lastSlot the last slot used in this lightpath
     * @param modulation the modulation id used in this lightpath
     * @return the EONLightPath object
     */
    @Override
    public EONLightPath createCandidateEONLightPath(int src, int dst, int[] links, int firstSlot, int lastSlot, int modulation) {
        return new EONLightPath(1, src, dst, links, firstSlot, lastSlot, modulation, EONPhysicalTopology.getSlotSize());
    }
}
