/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ons.ra;

import java.util.*;

import nfvs.*;
import nfvs.utils.Dijkstra.DijkstraDisaster;
import ons.EONLightPath;
import ons.EONLink;
import ons.EONPhysicalTopology;
import ons.Flow;
import ons.LightPath;
import ons.Modulation;
import ons.util.WeightedGraph;


public class EON_RA_SFC implements RA {

    private ControlPlaneForRA cp;
    private WeightedGraph graph;
    private int modulation;

    @Override
    public void simulationInterface(ControlPlaneForRA cp) {
        this.cp = cp;
        this.graph = cp.getPT().getWeightedGraph();
        //Set the default modulation
        this.modulation = Modulation._BPSK;
    }

    @Override
    public void flowArrival(Flow flow) {
        int[] nodes;
        int[] links;
        long id;
        LightPath[] lps = new LightPath[1];

        DijkstraDisaster dja = new DijkstraDisaster();

        Request request = (Request) flow;
        List<Integer> selectedDatacenters = new ArrayList<Integer>();
        List<Integer> datacentersPath = new ArrayList<Integer>();
        List<Integer> instatiatedFunctions = new ArrayList<Integer>();
        double rout_costs = 0;
        double setup_costs = 0;
        int fidx = 0;

        /*  First Stage Begin */
        for(int function: request.getRequestedFunctions()) {
            Datacenter min_dc = null;
            Datacenter current_dc = null;
            int min_setup_cost = Integer.MAX_VALUE;
            int current_cost = Integer.MAX_VALUE;
            VNF vnf;
            for(int k = 0; k < cp.getPT().getNumNodes(); k++) {
                current_dc = (Datacenter) cp.getPT().getNode(k);
                current_cost = current_dc.implementAtCost(function);
                if((current_cost < min_setup_cost) && (current_dc.hasResourcesTo(function) || (current_dc.hasInstantiatedFunctionWithCapacity(function) != null))) {
                    min_setup_cost = current_cost;
                    min_dc = current_dc;
                }
            }
            if(min_dc == null) {
                NFVStatistics.getObject().blockedByDC(request);
                request.freeFunctions();
                cp.blockFlow(flow.getID());
                return;
            }
            vnf = min_dc.hasInstantiatedFunctionWithCapacity(function);
            if(vnf == null) {
                vnf = min_dc.instantiateFunction(function);
                if(vnf == null) {
                    NFVStatistics.getObject().blockedByVNF(request);
                    request.freeFunctions();
                    cp.blockFlow(flow.getID());
                    return;
                }
                instatiatedFunctions.add(vnf.getID());
                setup_costs += vnf.getSetupCost();
            }
            vnf.newRequest();
            request.setUsedFunction(fidx++, vnf);
            selectedDatacenters.add(min_dc.getID());
        }
        /*  First Stage End */

        /*  Second Stage Begin */
        int src = flow.getSource();
        datacentersPath.add(src);
        for(int dst: selectedDatacenters) {
            /*  Second Stage */
            //if(!datacentersPath.contains(dst)) {
                nodes = dja.getShortestPath(graph,src,dst,cp.getPT(),flow.getRate());
                if((nodes.length < 1)) {
                    NFVStatistics.getObject().blockedByPath(request);
                    request.freeFunctions();
                    cp.blockFlow(flow.getID());
                    return;
                }
                for(int n = 1; n < nodes.length; ++n) {
                    datacentersPath.add(nodes[n]);
                }
                src = dst;
            //}
        }
        nodes = dja.getShortestPath(graph,src,request.getDestination(),cp.getPT(),flow.getRate());
        if(nodes.length < 1) {
            NFVStatistics.getObject().blockedByPath(request);
            request.freeFunctions();
            cp.blockFlow(flow.getID());
            return;
        }
        for(int n = 1; n < nodes.length; ++n) {
            datacentersPath.add(nodes[n]);
        }
        /*  Second Stage End */

        nodes = new int[datacentersPath.size()];
        for(int i = 0; i < nodes.length; ++i) {
            nodes[i] = datacentersPath.get(i);
        }
        datacentersPath.clear();

        // Create the links vector
        links = new int[nodes.length - 1];
        for (int j = 0; j < links.length; j++) {
            EONLinkE link = (EONLinkE)cp.getPT().getLink(nodes[j], nodes[j + 1]);
            //TODO: Verificar custos de roteamento dos links
            rout_costs += ((double)link.getSetupCost());// +  ((double)request.getRate() / (link.getAvaiableSlots()*EONPhysicalTopology.getSlotSize()));
            links[j] = link.getID();
        }


        for(int l : links) {
            if(cp.getPT().getLink(l).getWeight() >= Double.POSITIVE_INFINITY) {
                NFVStatistics.getObject().blockedByPath(request);
                request.freeFunctions();
                cp.blockFlow(flow.getID());
                return;
            }
        }


        // Calculates the required slots
        int requiredSlots = Modulation.convertRateToSlot(flow.getRate(), EONPhysicalTopology.getSlotSize(), modulation);

        // Evaluate if each link have space to the required slots
        Map<Integer, Integer> dls = new HashMap<Integer, Integer>();
        for (int i = 0; i < links.length; i++) {
            Integer av = dls.get(links[i]);
            if(av == null) {
                av = ((EONLink) cp.getPT().getLink(links[i])).getAvaiableSlots();
                dls.put(links[i], av);
            }
            if ((av - requiredSlots) < 0) {
                NFVStatistics.getObject().blockedByBW(request);
                request.freeFunctions();
                cp.blockFlow(flow.getID());
                return;
            }
            else {
                dls.put(links[i], av - requiredSlots);
            }
        }

        // Intersection First-Fit
        Set<Integer> slot_set =  ((EONLink)cp.getPT().getLink(links[0])).getSlotsAvailable(requiredSlots);
        for(int i = 1; i < links.length; ++i) {
            slot_set.retainAll(((EONLink)cp.getPT().getLink(links[i])).getSlotsAvailable(requiredSlots));
        }

        for(int firstSlot : slot_set) {
            // Now you create the lightpath to use the createLightpath VT
            EONLightPath lp = cp.createCandidateEONLightPath(flow.getSource(), flow.getDestination(), links,
                    firstSlot, (firstSlot + requiredSlots - 1), modulation);
            // Now you try to establish the new lightpath, accept the call
            if ((id = cp.getVT().createLightpath(lp)) >= 0) {
                // Single-hop routing (end-to-end lightpath)
                lps[0] = cp.getVT().getLightpath(id);
                if (cp.acceptFlow(flow.getID(), lps)) {
                    NFVStatistics.accept(request, rout_costs,instatiatedFunctions, links.length, setup_costs);
                    return;
                } else {
                    // Something wrong
                    // Dealocates the lightpath in VT and try again
                    cp.getVT().deallocatedLightpath(id);
                }
            }
        }
        // Block the call
        NFVStatistics.getObject().blockedByLP(request);
        cp.blockFlow(flow.getID());
        request.freeFunctions();
    }

    @Override
    public void flowDeparture(long id) {
        Request request = (Request) cp.getFlow(id);
        if(request != null) {
            request.freeFunctions();
        }
    }
}
