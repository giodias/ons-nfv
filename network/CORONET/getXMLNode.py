#!/usr/bin/python3

"""<vnf id="{0}" cost="50" capacity="2" >
	<resource id="0" cost="{1}"/>
	<resource id="1" cost="{2}"/>
	<resource id="2" cost="{3}"/>
</vnf>

id = 0
"""

EON = True

MIN_COST  = 30
MAX_COST  = 70
RESOURCES = 500
COST      = 50
NODES     = 75

out = open('nodes_xml.txt','w')

for node in range(NODES):
	out.write('<node id="{0}" grooming-in-ports="32" grooming-out-ports="32"'.format(node))
	if(not EON):
	    out.write(' wlconverters="0" wlconversion-range="0" >\n')
	else:
	    out.write(' >\n')
	for rid in range(0, 3):
		out.write('    <resources id="{0}" value="{1}"/>\n'.format(rid, RESOURCES))
	for vid in range(0,5):
		out.write('    <vnf id="{0}" cost="{1}" capacity="2" >\n'.format(vid, COST))
		for rid in range(0,3):
			out.write('        <resource id="{0}" cost_min="{1}" cost_max="{2}"/>\n'.format(rid, MIN_COST, MAX_COST))
		out.write('    </vnf>\n')
	out.write('</node>\n')
out.close()
