#!/bin/bash
MIN=5
STEP=5
MAX=100
REQUESTS=60
#
if [ $# -ne 2 ]
then
    echo "Param Error"
    exit
else
    FILEI=$1
    OUTP=$2
fi
#
for ((erlang=MIN;erlang<=MAX;erlang+=STEP))
do
    
    STR="s/(|.+\/)(.+)\.xml/\\2_"
    STR+=$erlang
    STR+="_RESULT/"
    FO=$OUTP
    FO+="/"
    FO+=$(echo $FILEI | sed -E $STR)
    echo -e "#Blocked_Requests\tBR\tBBR\tRouting_Cost\tUsed_Links\tSetup_Costs\tSatisfied_Functions\tInstanced_Functions\tBlocked by DC\tBlocked by VNF\tBlocked by BW\tBlocked by Path" > $FO
 
    for seed in 1 3 6 8 11 13 16 18 21 23
    do   
    
        aux=$(java -jar ons_nfv.jar $FILEI $seed $REQUESTS $erlang $erlang 1)

        out=$( echo $aux | grep -om 1 -E " Blocked Requests: ([0-9]+)"          | head -n1)
        out+=$(echo $aux | grep -om 1 -E " BR : ([0-9]*[\.,]?[0-9]+)%"          | head -n1)
        out+=$(echo $aux | grep -om 1 -E " BBR : ([0-9]*[\.,]?[0-9]+)%"         | head -n1)
        out+=$(echo $aux | grep -om 1 -E " Routing Cost: ([0-9]*[\.,]?[0-9]+)"  | head -n1)
        out+=$(echo $aux | grep -om 1 -E " Used Links: ([0-9]*[\.,]?[0-9]+)"    | head -n1)
        out+=$(echo $aux | grep -om 1 -E " Setup Costs: ([0-9]*[\.,]?[0-9]+)"   | head -n1)
        out+=$(echo $aux | grep -om 1 -E " Total Satisfied Functions: ([0-9]+)" | head -n1)
        out+=$(echo $aux | grep -om 1 -E " Total Instanced Functions: ([0-9]+)" | head -n1)
        out+=$(echo $aux | grep -om 1 -E " Blocked by DC: ([0-9]+)"   | head -n1)
        out+=$(echo $aux | grep -om 1 -E " Blocked by VNF: ([0-9]+)"  | head -n1)
        out+=$(echo $aux | grep -om 1 -E " Blocked by BW: ([0-9]+)"   | head -n1)
        out+=$(echo $aux | grep -om 1 -E " Blocked by Path: ([0-9]+)" | head -n1)
        out=$(echo $out  | sed -Er "s/Blocked Requests: (.+) BR : (.+)% BBR : (.+)% Routing Cost: (.+) Used Links: (.+) Setup Costs: (.+) Total Satisfied Functions: ([0-9]+) Total Instanced Functions: ([0-9]+) Blocked by DC: ([0-9]+)/\\1\t\\2\t\\3\t\\4\t\\5\t\\6\t\\7\t\\8\t\\9\t/")
        out=$(echo $out  | sed -Er "s/Blocked by VNF: ([0-9]+) Blocked by BW: ([0-9]+) Blocked by Path: ([0-9]+)/\\1\t\\2\t\\3/")
        
        echo -e $out >> $FO
    done
    echo "$erlang erlangs Done!"
done


