#!/usr/bin/gnuplot

# TITLEBASE OUTPUT TITLE1 FILE1 TITLE2 FILE2

#Constants
OUTPATH="../GRAPH/CORONET/RecMigration/eps/"
FILE1="../DATA/CORONET_RSM.data"
FILE2="../DATA/CORONET_RCM.data"

TITLE1="Recovery with migrations"
TITLE2="Recovery without migrations"
EXTENSION=".eps"

TITLEBASE="Recovery with Migrations"

#Initial Configuration
if (EXTENSION eq ".eps") {
	set term post eps enhanced color "sans-serif" 14
} else {
	if (EXTENSION eq ".png"){
		set term png size 1024,768
	} else {
		print("Invalid File Extension")
		exit()
	}
}

set xlabel "Load (Erlangs)"

#Histogram
set boxwidth 0.9 relative
set style data histogram
set style fill solid 1.0 border -1
set bars fullwidth
set style histogram errorbars gap 2 lw 2

set style fill pattern 0

#Rout Cost
set title  TITLEBASE."\nRouting Cost"
set ylabel "Routing Cost"
set output OUTPATH."Routing_Cost".EXTENSION
plot FILE1 using 40:41:xticlabels(1) title TITLE1."(Routing Cost)", \
	 FILE2 using 40:41 title TITLE2."(Routing Cost)"

set key left top

#BBR
set title  TITLEBASE."\nBBR"
set ylabel "BBR (%)"
set output OUTPATH."BBR".EXTENSION
plot FILE1 using 24:25:xticlabels(1) title TITLE1, \
     FILE2 using 24:25 title TITLE2

#BR
set title  TITLEBASE."\nBR"
set ylabel "BR (%)"
set output OUTPATH."BR".EXTENSION
plot FILE1 using 26:27:xticlabels(1) title TITLE1, \
     FILE2 using 26:27 title TITLE2


#Blocked Requests (COS)
set title  TITLEBASE."\nBP (COS)"
set ylabel "BP (%)"
set output OUTPATH."BP_COS".EXTENSION
plot FILE1 using 2:3:xticlabels(1) title TITLE1."(Hard Real-Time)", \
	 FILE1 using 4:5 title TITLE1."(Soft Bandwidth)", \
	 FILE1 using 6:7 title TITLE1."(Soft Delay)", \
	 FILE1 using 8:9 title TITLE1."(Non Real-Time)", \
	 FILE2 using 2:3 title TITLE2."(Hard Real-Time)", \
	 FILE2 using 4:5 title TITLE2."(Soft Bandwidth)", \
	 FILE2 using 6:7 title TITLE2."(Soft Delay)", \
	 FILE2 using 8:9 title TITLE2."(Non Real-Time)"

#Disaster Requests(COS)
set title  TITLEBASE."\nDP (COS)"
set ylabel "DP (%)"
set output OUTPATH."DP_COS".EXTENSION
plot FILE1 using 10:11:xticlabels(1) title TITLE1."(Hard Real-Time)", \
	 FILE1 using 12:13 title TITLE1."(Soft Bandwidth)", \
	 FILE1 using 14:15 title TITLE1."(Soft Delay)", \
	 FILE1 using 16:17 title TITLE1."(Non Real-Time)", \
	 FILE2 using 10:11 title TITLE2."(Hard Real-Time)", \
	 FILE2 using 12:13 title TITLE2."(Soft Bandwidth)", \
	 FILE2 using 14:15 title TITLE2."(Soft Delay)", \
	 FILE2 using 16:17 title TITLE2."(Non Real-Time)"

#Blocked Requests
set title  TITLEBASE."\nBlocked Requests"
set ylabel "Blocked (%)"
set output OUTPATH."Blocked".EXTENSION
plot FILE1 using 28:29:xticlabels(1) title TITLE1." By DC", \
     FILE1 using 30:31 title TITLE1." By VNF" , \
	 FILE1 using 32:33 title TITLE1." By BW"  , \
	 FILE1 using 34:35 title TITLE1." By Path", \
     FILE1 using 36:37 title TITLE1." By LP", \
	 FILE2 using 28:29 title TITLE2." By DC", \
     FILE2 using 30:31 title TITLE2." By VNF" , \
	 FILE2 using 32:33 title TITLE2." By BW"  , \
	 FILE2 using 34:35 title TITLE2." By Path", \
     FILE2 using 36:37 title TITLE2." By LP"

#Recovery Requests
set title  TITLEBASE."\nRecovery (COS)"
set ylabel "Recovery (%)"
set output OUTPATH."Recovery_COS".EXTENSION
plot FILE1 using 42:43:xticlabels(1) title TITLE1."(Hard Real-Time)", \
	 FILE1 using 44:45 title TITLE1."(Soft Bandwidth)", \
	 FILE1 using 46:47 title TITLE1."(Soft Delay)", \
	 FILE1 using 48:49 title TITLE1."(Non Real-Time)", \
	 FILE2 using 42:43 title TITLE2."(Hard Real-Time)", \
	 FILE2 using 44:45 title TITLE2."(Soft Bandwidth)", \
	 FILE2 using 46:47 title TITLE2."(Soft Delay)", \
	 FILE2 using 48:49 title TITLE2."(Non Real-Time)"

#Cost
set title  TITLEBASE."\nBlocked Cost"
set ylabel "Cost ($)"
set output OUTPATH."Blocked_Cost".EXTENSION
plot FILE1 using 18:19:xticlabels(1) title TITLE1."(Blocked Cost)", \
	 FILE2 using 18:19 title TITLE2."(Blocked Cost)"
     
set title  TITLEBASE."\nDisaster Cost"  
set output OUTPATH."Disaster_Cost".EXTENSION
plot FILE1 using 20:21:xticlabels(1) title TITLE1."(Disaster Cost)", \
     FILE2 using 20:21 title TITLE2."(Disaster Cost)"

set title  TITLEBASE."\nTotal Cost"
set output OUTPATH."Total_Cost".EXTENSION
plot FILE1 using 22:23:xticlabels(1) title TITLE1."(TOTAL)", \
	 FILE2 using 22:23 title TITLE2."(TOTAL)"

set title TITLEBASE."\nInterrupted Requests"
set ylabel "Interrupted/Total (%)"
set output OUTPATH."Interrupted_Requests".EXTENSION
plot FILE1 using 50:51:xticlabels(1) title TITLE1."(Hard Real-Time)", \
     FILE1 using 52:53 title TITLE1."(Soft Bandwidth)", \
     FILE1 using 54:55 title TITLE1."(Soft Delay)", \
     FILE1 using 56:57 title TITLE1."(Non Real-Time)", \
     FILE2 using 50:51 title TITLE2."(Hard Real-Time)", \
     FILE2 using 52:53 title TITLE2."(Soft Bandwidth)", \
     FILE2 using 54:55 title TITLE2."(Soft Delay)", \
     FILE2 using 55:57 title TITLE2."(Non Real-Time)"
exit

