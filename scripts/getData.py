#!/usr/bin/env python3
import subprocess
import os
import re
import statistics
import argparse
import math
import time
#Arg Parser
parser = argparse.ArgumentParser(description='Coleta e trata os dados obtidos atraves de simulações no ONS')
parser.add_argument('ons_jar', default="ons_nfv.jar", type=str)
parser.add_argument('xml', type=str)
parser.add_argument('output', default="RESULT.data", type=str)
parser.add_argument('requests', type=int)
parser.add_argument('load_min', type=int)
parser.add_argument('load_max', type=int)
parser.add_argument('load_step', type=int)

args = parser.parse_args()

ons=args.ons_jar
xml=args.xml
ofile=args.output
requests=args.requests
load_m=args.load_min
load_M=args.load_max
load_step=args.load_step

confidence=1.96
load_separator = "*****"
erlangs = int(math.floor(((load_M - load_m)/load_step)+1))
#

#BP DP BC DC BBR BR

#re
BP0p   = re.compile(r"BP-0 ([0-9]*[\.,]?[0-9]+)\%")
BP1p   = re.compile(r"BP-1 ([0-9]*[\.,]?[0-9]+)\%")
BP2p   = re.compile(r"BP-2 ([0-9]*[\.,]?[0-9]+)\%")
BP3p   = re.compile(r"BP-3 ([0-9]*[\.,]?[0-9]+)\%")
DP0p   = re.compile(r"DP-0 ([0-9]*[\.,]?[0-9]+)\%")
DP1p   = re.compile(r"DP-1 ([0-9]*[\.,]?[0-9]+)\%")
DP2p   = re.compile(r"DP-2 ([0-9]*[\.,]?[0-9]+)\%")
DP3p   = re.compile(r"DP-3 ([0-9]*[\.,]?[0-9]+)\%")
BCp    = re.compile(r"Blocked Cost: ([0-9]*[\.,]?[0-9]+)")
DCp    = re.compile(r"Disaster Cost: ([0-9]*[\.,]?[0-9]+)")
TCp    = re.compile(r"Total Cost: ([0-9]*[\.,]?[0-9]+)")
BBRp   = re.compile(r"BBR[\s \t]+: ([0-9]*[\.,]?[0-9]+)")
BRp    = re.compile(r"\nBR[\s \t]+: ([0-9]*[\.,]?[0-9]+)")
BbDCp  = re.compile(r"Blocked by DC: ([0-9]*[\.,]?[0-9]+)")
BbVNFp = re.compile(r"Blocked by VNF: ([0-9]*[\.,]?[0-9]+)")
BbBWp  = re.compile(r"Blocked by BW: ([0-9]*[\.,]?[0-9]+)")
BbPp   = re.compile(r"Blocked by Path: ([0-9]*[\.,]?[0-9]+)")
BbLPp  = re.compile(r"Blocked by LP: ([0-9]*[\.,]?[0-9]+)")
SCp    = re.compile(r"Setup Costs: ([0-9]*[\.,]?[0-9]+)")
RCp    = re.compile(r"Routing Cost: ([0-9]*[\.,]?[0-9]+)")
RP0p   = re.compile(r"RP-0 ([0-9]*[\.,]?[0-9]+)\%")
RP1p   = re.compile(r"RP-1 ([0-9]*[\.,]?[0-9]+)\%")
RP2p   = re.compile(r"RP-2 ([0-9]*[\.,]?[0-9]+)\%")
RP3p   = re.compile(r"RP-3 ([0-9]*[\.,]?[0-9]+)\%")
AD0p   = re.compile(r"AD-0 ([0-9]*[\.,]?[0-9]+)\%")
AD1p   = re.compile(r"AD-1 ([0-9]*[\.,]?[0-9]+)\%")
AD2p   = re.compile(r"AD-2 ([0-9]*[\.,]?[0-9]+)\%")
AD3p   = re.compile(r"AD-3 ([0-9]*[\.,]?[0-9]+)\%")

BP0   = [[] for i in range(erlangs)]
BP1   = [[] for i in range(erlangs)]
BP2   = [[] for i in range(erlangs)]
BP3   = [[] for i in range(erlangs)]
DP0   = [[] for i in range(erlangs)]
DP1   = [[] for i in range(erlangs)]
DP2   = [[] for i in range(erlangs)]
DP3   = [[] for i in range(erlangs)]
DP    = [[] for i in range(erlangs)]
BC    = [[] for i in range(erlangs)]
DC    = [[] for i in range(erlangs)]
TC    = [[] for i in range(erlangs)]
BBR   = [[] for i in range(erlangs)]
BR    = [[] for i in range(erlangs)]
BbDC  = [[] for i in range(erlangs)]
BbVNF = [[] for i in range(erlangs)]
BbBW  = [[] for i in range(erlangs)]
BbP   = [[] for i in range(erlangs)]
BbLP  = [[] for i in range(erlangs)]
SC    = [[] for i in range(erlangs)]
RC    = [[] for i in range(erlangs)]
RP0   = [[] for i in range(erlangs)]
RP1   = [[] for i in range(erlangs)]
RP2   = [[] for i in range(erlangs)]
RP3   = [[] for i in range(erlangs)]
AD0   = [[] for i in range(erlangs)]
AD1   = [[] for i in range(erlangs)]
AD2   = [[] for i in range(erlangs)]
AD3   = [[] for i in range(erlangs)]

seeds = [1,2,3,4,5,6,7,8,9,10]

#log = open('log.txt', "w")

for seed in seeds:
    r = []
    outpt = subprocess.run(['java','-jar',ons, xml, str(seed), str(requests), str(load_m), str(load_M), str(load_step)], check=True, stdout=subprocess.PIPE, universal_newlines=True).stdout
    simR = outpt.split(load_separator)
    #log.write(outpt)
    #log.write('\n\n')
    for i in range(len(simR)-1):
    #BP
        #BP0
        g = BP0p.search(simR[i+1]).groups()
        BP0[i].append(float(g[0]))
        #BP1
        g = BP1p.search(simR[i+1]).groups()
        BP1[i].append(float(g[0]))
        #BP2
        g = BP2p.search(simR[i+1]).groups()
        BP2[i].append(float(g[0]))
        #BP3
        g = BP3p.search(simR[i+1]).groups()
        BP3[i].append(float(g[0]))
    #DP
        #DP0
        g = DP0p.search(simR[i+1]).groups()
        DP0[i].append(float(g[0]))
        #DP1
        g = DP1p.search(simR[i+1]).groups()
        DP1[i].append(float(g[0]))
        #DP2
        g = DP2p.search(simR[i+1]).groups()
        DP2[i].append(float(g[0]))
        #DP3
        g = DP3p.search(simR[i+1]).groups()
        DP3[i].append(float(g[0]))
    #Blocked Cost
        g = BCp.search(simR[i+1]).groups()
        BC[i].append(float(g[0]))
    #Disaster Cost
        g = DCp.search(simR[i+1]).groups()
        DC[i].append(float(g[0]))
    #BBR
        g = BBRp.search(simR[i+1]).groups()
        BBR[i].append(float(g[0]))
    #BR
        g = BRp.search(simR[i+1]).groups()
        BR[i].append(float(g[0]))
    #Blocked by Data Center
        g = BbDCp.search(simR[i+1]).groups()
        BbDC[i].append(float(g[0]))
    #Blocked by VNF
        g = BbVNFp.search(simR[i+1]).groups()
        BbVNF[i].append(float(g[0]))
    #Blocked by Bandwidth
        g = BbBWp.search(simR[i+1]).groups()
        BbBW[i].append(float(g[0]))
    #Blocked by Path
        g = BbPp.search(simR[i+1]).groups()
        BbP[i].append(float(g[0]))
    #Blocked by LP
        g = BbLPp.search(simR[i+1]).groups()
        BbLP[i].append(float(g[0]))
    #Setup Cost
        g = SCp.search(simR[i+1]).groups()
        SC[i].append(float(g[0]))
    #Recovery Cost
        g = RCp.search(simR[i+1]).groups()
        RC[i].append(float(g[0]))
    #Total Cost
        g = TCp.search(simR[i+1]).groups()
        TC[i].append(float(g[0]))
    #RP
        #RP0
        g = RP0p.search(simR[i+1]).groups()
        RP0[i].append(float(g[0]))
        #RP1
        g = RP1p.search(simR[i+1]).groups()
        RP1[i].append(float(g[0]))
        #RP2
        g = RP2p.search(simR[i+1]).groups()
        RP2[i].append(float(g[0]))
        #RP3
        g = RP3p.search(simR[i+1]).groups()
        RP3[i].append(float(g[0]))
    #AD
        #AD0
        g = AD0p.search(simR[i+1]).groups()
        AD0[i].append(float(g[0]))
        #AD1
        g = AD1p.search(simR[i+1]).groups()
        AD1[i].append(float(g[0]))
        #AD2
        g = AD2p.search(simR[i+1]).groups()
        AD2[i].append(float(g[0]))
        #AD3
        g = AD3p.search(simR[i+1]).groups()
        AD3[i].append(float(g[0]))
    print("Seed {0} done!".format(seed))
    time.sleep(1.5)
#process
#log.close()

STDEV_MUL = confidence/math.sqrt(len(seeds)) 

fout = open(ofile,"w")
fout.write("#load\tBP0\tdB0\tBP1\tdB1\tBP2\tdB2\tBP3\tdB3\tDP0\tdD0\tDP1\tdD1\tDP2\tdD2\tDP3\tdD3\tBKC\tdBC\tDSC\tdDC\tTC\tdTC\tBBR\tdBBR\tBR\tdBR\tBbDC\tdBbDC\tBbVNF\tdBbVNF\tBbBW\tdBbBW\tBbP\tdBbP\tBbLP\tdBbLP\tSC\tdSC\tRC\tdRC\tRP0\tdR0\tRP1\tdR1\tRP2\tdR2\tRP3\tdR3\tAD0\tdA0\tAD1\tdA1\tAD2\tdA2\tAD3\tdA3\n")
for i in range(erlangs):
    fout.write("{0}\t{1:.6f}\t{2:.6f}\t".format(int(load_m+(i*load_step)), statistics.mean(BP0[i]), STDEV_MUL * statistics.pstdev(BP0[i])))
    fout.write("{0:.6f}\t{1:.6f}\t".format(statistics.mean(BP1[i])  , STDEV_MUL * statistics.pstdev(BP1[i])))
    fout.write("{0:.6f}\t{1:.6f}\t".format(statistics.mean(BP2[i])  , STDEV_MUL * statistics.pstdev(BP2[i])))
    fout.write("{0:.6f}\t{1:.6f}\t".format(statistics.mean(BP3[i])  , STDEV_MUL * statistics.pstdev(BP3[i])))
    #
    fout.write("{0:.6f}\t{1:.6f}\t".format(statistics.mean(DP0[i])  , STDEV_MUL * statistics.pstdev(DP0[i])))
    fout.write("{0:.6f}\t{1:.6f}\t".format(statistics.mean(DP1[i])  , STDEV_MUL * statistics.pstdev(DP1[i])))
    fout.write("{0:.6f}\t{1:.6f}\t".format(statistics.mean(DP2[i])  , STDEV_MUL * statistics.pstdev(DP2[i])))
    fout.write("{0:.6f}\t{1:.6f}\t".format(statistics.mean(DP3[i])  , STDEV_MUL * statistics.pstdev(DP3[i])))
    #
    fout.write("{0:.6f}\t{1:.6f}\t".format(statistics.mean(BC[i])   , STDEV_MUL * statistics.pstdev(BC[i])))
    fout.write("{0:.6f}\t{1:.6f}\t".format(statistics.mean(DC[i])   , STDEV_MUL * statistics.pstdev(DC[i])))
    fout.write("{0:.6f}\t{1:.6f}\t".format(statistics.mean(TC[i])   , STDEV_MUL * statistics.pstdev(TC[i])))    
    #
    fout.write("{0:.6f}\t{1:.6f}\t".format(statistics.mean(BBR[i])  , STDEV_MUL * statistics.pstdev(BBR[i])))
    fout.write("{0:.6f}\t{1:.6f}\t".format(statistics.mean(BR[i])   , STDEV_MUL * statistics.pstdev(BR[i])))
    #
    fout.write("{0:.6f}\t{1:.6f}\t".format(statistics.mean(BbDC[i]) , STDEV_MUL * statistics.pstdev(BbDC[i])))
    fout.write("{0:.6f}\t{1:.6f}\t".format(statistics.mean(BbVNF[i]), STDEV_MUL * statistics.pstdev(BbVNF[i])))
    fout.write("{0:.6f}\t{1:.6f}\t".format(statistics.mean(BbBW[i]) , STDEV_MUL * statistics.pstdev(BbBW[i])))
    fout.write("{0:.6f}\t{1:.6f}\t".format(statistics.mean(BbP[i])  , STDEV_MUL * statistics.pstdev(BbP[i])))
    fout.write("{0:.6f}\t{1:.6f}\t".format(statistics.mean(BbLP[i]) , STDEV_MUL * statistics.pstdev(BbLP[i])))
    #
    fout.write("{0:.6f}\t{1:.6f}\t".format(statistics.mean(SC[i])   , STDEV_MUL * statistics.pstdev(SC[i])))
    fout.write("{0:.6f}\t{1:.6f}\t".format(statistics.mean(RC[i])   , STDEV_MUL * statistics.pstdev(RC[i])))
    #
    fout.write("{0:.6f}\t{1:.6f}\t".format(statistics.mean(RP0[i])  , STDEV_MUL * statistics.pstdev(RP0[i])))
    fout.write("{0:.6f}\t{1:.6f}\t".format(statistics.mean(RP1[i])  , STDEV_MUL * statistics.pstdev(RP1[i])))
    fout.write("{0:.6f}\t{1:.6f}\t".format(statistics.mean(RP2[i])  , STDEV_MUL * statistics.pstdev(RP2[i])))
    fout.write("{0:.6f}\t{1:.6f}\t".format(statistics.mean(RP3[i])  , STDEV_MUL * statistics.pstdev(RP3[i])))
    #
    fout.write("{0:.6f}\t{1:.6f}\t".format(statistics.mean(AD0[i])  , STDEV_MUL * statistics.pstdev(AD0[i])))
    fout.write("{0:.6f}\t{1:.6f}\t".format(statistics.mean(AD1[i])  , STDEV_MUL * statistics.pstdev(AD1[i])))
    fout.write("{0:.6f}\t{1:.6f}\t".format(statistics.mean(AD2[i])  , STDEV_MUL * statistics.pstdev(AD2[i])))
    fout.write("{0:.6f}\t{1:.6f}\n".format(statistics.mean(AD3[i])  , STDEV_MUL * statistics.pstdev(AD3[i])))
fout.close()

