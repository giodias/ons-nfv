#!/usr/bin/gnuplot

FILEPATH="../DATA/EON_RA_COS_2_ERLANGS/"
FILEBASE="DATA_"
FILEPOSF="_ERLANGS"
CONFIDENCE_LEVEL=1.96

set print "-"

print "#BP0 dB0 BP1 dB1 BP2 dB2 BP3 dB3 DP0 dD0 DP1 dD1 DP2 dD2 DP3 dD3 BKC dBC DSC dDC BBR dBBR BR dBR"

do for [r in "50 90 130 170 210"] {
    out=r  
    file=FILEPATH.FILEBASE.r.FILEPOSF
    do for [c=1:12:1] {
        stats file using c nooutput
        out=out.sprintf(" %f %f", STATS_mean, CONFIDENCE_LEVEL*STATS_mean_err)
    }
    print out
}

exit
